package Main;

import QAP.*;

import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by mpancorbo on 12/09/14.
 */
public class Practica1 {
    public static final int GREEDY = 1;
    public static final int BUSQUEDA_LOCAL = 2;
    public static final int BUSQUEDA_TABU = 3;
    public static final String[] problem = {"Els19", "Chr20a", "Chr25a", "Nug25", "Bur26a", "Bur26b", "Tai30a", "Tai30b", "Esc32a", "Kra32", "Tai35a", "Tai35b", "Tho40", "Tai40a", "Sko42", "Sko49", "Tai50a", "Tai50b", "Tai60a", "Lipa90a", "Todos los problemas"};

    public static void main(String[ ] args){

        System.out.println("Práctica 1. Metaheurísticas");
        System.out.println();

        int tipoSolucion = menuTipoSolucion();
        int problemIndex = menuProblemIndex()-1;
        long semilla = 0;
        int numIteration = 0;
        if(tipoSolucion != Practica1.GREEDY){
            if(tipoSolucion != 4 && problemIndex != 20)
                numIteration = menuIteration();
            semilla = menuSemilla();
        }
        if(problemIndex!=20) {
            try {
                AbstractSolucion solucion;
                String path = "data/" + problem[problemIndex].toLowerCase() + ".dat";
                ModelSolucion[] soluciones;
                switch (tipoSolucion) {
                    case Practica1.GREEDY:
                        solucion = new SolucionGreedy(path);
                        ModelSolucion solucionGreedy = getNSoluciones(solucion, 1)[0];
                        printResultadosGreedy(solucionGreedy);
                        break;
                    case Practica1.BUSQUEDA_LOCAL:
                        solucion = new SolucionBusquedaLocal(path, semilla);
                        soluciones = getNSoluciones(solucion, numIteration);
                        printResultadosProbabilistico(soluciones);
                        break;
                    case Practica1.BUSQUEDA_TABU:
                        solucion = new SolucionBusquedaTabu(path, semilla);
                        soluciones = getNSoluciones(solucion, numIteration);
                        printResultadosProbabilistico(soluciones);
                        break;
                    case 4:
                        soluciones = new ModelSolucion[3];
                        solucion = new SolucionGreedy(path);
                        soluciones[0] = getNSoluciones(solucion, 1)[0];
                        solucion = new SolucionBusquedaLocal(path, semilla);
                        soluciones[1] = getNSoluciones(solucion, 1)[0];
                        solucion = new SolucionBusquedaTabu(path, semilla);
                        soluciones[2] = getNSoluciones(solucion, 1)[0];
                        printComparativa(soluciones);
                        break;
                }

            } catch (FileNotFoundException exc) {
                System.out.println("La ruta especificada para el archivo no existe: " + exc.getMessage());
            }
        }else{
            try {
                AbstractSolucion solucion;
                ModelSolucion[] soluciones;
                String path;
                switch (tipoSolucion) {
                    case Practica1.GREEDY:
                        soluciones = new ModelSolucion[20];
                        for(int i=0;i<soluciones.length;i++){
                            path= "data/" + problem[i].toLowerCase() + ".dat";
                            solucion = new SolucionGreedy(path);
                            soluciones[i] = getNSoluciones(solucion, 1)[0];
                        }
                        printResultadosTodos(soluciones);
                        break;
                    case Practica1.BUSQUEDA_LOCAL:
                        soluciones = new ModelSolucion[20];
                        for(int i=0;i<soluciones.length;i++){
                            path= "data/" + problem[i].toLowerCase() + ".dat";
                            solucion = new SolucionBusquedaLocal(path, semilla);
                            soluciones[i] = getNSoluciones(solucion, 1)[0];
                        }
                        printResultadosTodos(soluciones);
                        break;
                    case Practica1.BUSQUEDA_TABU:
                        soluciones = new ModelSolucion[20];
                        for(int i=0;i<soluciones.length;i++){
                            path= "data/" + problem[i].toLowerCase() + ".dat";
                            solucion = new SolucionBusquedaTabu(path, semilla);
                            soluciones[i] = getNSoluciones(solucion, 1)[0];
                        }
                        printResultadosTodos(soluciones);
                        break;
                    case 4:
                        ModelSolucion[] solucionesG = new ModelSolucion[20];
                        ModelSolucion[] solucionesL = new ModelSolucion[20];
                        ModelSolucion[] solucionesT = new ModelSolucion[20];
                        for(int i=0;i<solucionesG.length;i++){
                            path= "data/" + problem[i].toLowerCase() + ".dat";
                            solucion = new SolucionGreedy(path);
                            solucionesG[i] = getNSoluciones(solucion, 1)[0];
                            solucion = new SolucionBusquedaLocal(path, semilla);
                            solucionesL[i] = getNSoluciones(solucion, 1)[0];
                            solucion = new SolucionBusquedaTabu(path, semilla);
                            solucionesT[i] = getNSoluciones(solucion, 1)[0];
                        }
                        printComparativaTotal(solucionesG,solucionesL,solucionesT);
                        break;
                }

            } catch (FileNotFoundException exc) {
                System.out.println("La ruta especificada para el archivo no existe: " + exc.getMessage());
            }
        }


    }


    public static int menuTipoSolucion(){
        Scanner in = new Scanner(System.in);
        int tipo;
        do {
            System.out.println("Escoge el método a usar para resolver el problema:");
            System.out.println("\t" + Practica1.GREEDY + "- Greedy");
            System.out.println("\t"+ Practica1.BUSQUEDA_LOCAL +"- Búsqueda Local");
            System.out.println("\t"+ Practica1.BUSQUEDA_TABU +"- Búsqueda Tabú");
            System.out.println("\t"+ "4- Comparativa con todos los métodos");

            tipo = in.nextInt();
        }while(tipo < 1 || tipo > 4);

        return tipo;
    }

    public static int menuProblemIndex(){
        Scanner in = new Scanner(System.in);
        int problemIndex;
        do{
            System.out.println("Escoge el problema que quieres resolver:");
            for(int i = 0; i < problem.length; i++){
                System.out.println("\t" + (i+1) + "- " + problem[i]);
            }
            problemIndex = in.nextInt();

        }while(problemIndex > problem.length || problemIndex < 1);

        return problemIndex;
    }
    public static int menuIteration(){
        Scanner in = new Scanner(System.in);
        int numIteration;
        System.out.println("Escoge el numero de iteraciones a realizar:");
        numIteration = in.nextInt();
        return numIteration;
    }

    public static long menuSemilla(){
        Scanner in = new Scanner(System.in);
        long semilla;
        System.out.println("Escoge la semilla para la solución inicial:");
        semilla = in.nextLong();

        return semilla;
    }

    public static void printResultadosGreedy(ModelSolucion soluciones){
        System.out.println(" Solución ");
        System.out.println("----------");
        System.out.println();
        soluciones.printSolucion();
        System.out.println("Coste: " + soluciones.getCoste());
        System.out.println("Tiempo empleado: " + soluciones.getTimeElapsed());
        System.out.println();
    }

    public static void printResultadosTodos(ModelSolucion[] soluciones){
        for(int i=0;i<soluciones.length;i++){
            System.out.println(" Solución para " + problem[i]);
            System.out.println("------------------------------");
            System.out.println();
            soluciones[i].printSolucion();
            System.out.println("Coste: " + soluciones[i].getCoste());
            System.out.println("Tiempo empleado: " + soluciones[i].getTimeElapsed());
            System.out.println();
        }
    }

    public static void printResultadosProbabilistico(ModelSolucion[] soluciones){
        ModelSolucion mejor = soluciones[0];
        ModelSolucion peor = soluciones[0];

        for (int i = 0; i < soluciones.length; i++) {
            System.out.println("Solución " + (i + 1));
            System.out.println("------------------");
            System.out.println();

            soluciones[i].printSolucion();
            System.out.println("Coste: " + soluciones[i].getCoste());
            System.out.println("Tiempo empleado: " + soluciones[i].getTimeElapsed());

            System.out.println();
            System.out.println();

            if(soluciones[i].getCoste() > peor.getCoste())
                peor = soluciones[i];
            else if(soluciones[i].getCoste() < mejor.getCoste())
                mejor = soluciones[i];
        }

        System.out.println("Coste de la mejor solución: " + mejor.getCoste());
        System.out.println("Coste de la peor solución: " + peor.getCoste());

        double desviacion = Estadisticas.desviacionMedia(soluciones, mejor);
        double tiempoMedio = Estadisticas.tiempoMedio(soluciones);
        System.out.println("Desviación media: " + desviacion);
        System.out.println("Tiempo medio: " + tiempoMedio);
    }



    private static void printComparativa(ModelSolucion[] soluciones) {
        System.out.println("Solución Greedy");
        System.out.println("---------------");
        System.out.println();

        soluciones[0].printSolucion();
        System.out.println("Coste: " + soluciones[0].getCoste());
        System.out.println("Tiempo empleado: " + soluciones[0].getTimeElapsed());

        System.out.println();
        System.out.println();

        System.out.println("Solución Búsqueda Local");
        System.out.println("-----------------------");
        System.out.println();

        soluciones[1].printSolucion();
        System.out.println("Coste: " + soluciones[1].getCoste());
        System.out.println("Tiempo empleado: " + soluciones[1].getTimeElapsed());

        System.out.println();
        System.out.println();

        System.out.println("Solución Búsqueda Tabú");
        System.out.println("----------------------");
        System.out.println();

        soluciones[2].printSolucion();
        System.out.println("Coste: " + soluciones[2].getCoste());
        System.out.println("Tiempo empleado: " + soluciones[2].getTimeElapsed());

        System.out.println();
        System.out.println();
    }

    private static void printComparativaTotal(ModelSolucion[] solucionesG, ModelSolucion[] solucionesL, ModelSolucion[] solucionesT) {
        for(int i = 0; i < solucionesG.length; i++){
            System.out.println("-------------------- ");
            System.out.println("  Problema "+ problem[i]);
            System.out.println("-------------------- ");
            System.out.println();

            System.out.println("Solución Greedy");
            System.out.println("---------------");
            solucionesG[i].printSolucion();
            System.out.println("Coste: " + solucionesG[i].getCoste());
            System.out.println("Tiempo empleado: " + solucionesG[i].getTimeElapsed());
            System.out.println();

            System.out.println("Solución Búsqueda Local");
            System.out.println("-----------------------");
            solucionesL[i].printSolucion();
            System.out.println("Coste: " + solucionesL[i].getCoste());
            System.out.println("Tiempo empleado: " + solucionesL[i].getTimeElapsed());
            System.out.println();

            System.out.println("Solución Búsqueda Tabú");
            System.out.println("----------------------");
            solucionesT[i].printSolucion();
            System.out.println("Coste: " + solucionesT[i].getCoste());
            System.out.println("Tiempo empleado: " + solucionesT[i].getTimeElapsed());
            System.out.println();
            System.out.println();
        }
    }

    private static ModelSolucion[] getNSoluciones(AbstractSolucion solucion, int n) throws FileNotFoundException{
        ModelSolucion[] soluciones = new ModelSolucion[n];
        for(int i = 0; i < n; i++)
            soluciones[i] = solucion.getSolucion();

        return soluciones;
    }
}
