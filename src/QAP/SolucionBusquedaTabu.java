package QAP;

import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Created by mpancorbo on 08/10/14.
 */
public class SolucionBusquedaTabu extends AbstractSolucion{

    private static final int ESTRATEGIA_INTENSIFICACION = 25;
    private static final int ESTRATEGIA_DIVERSIFICACION = 50;
    private static final int ESTRATEGIA_DIVERSIFICACION_CONTROLADA = 100;

    private TabuList listaTabu;
    private int[] mejorSolucionEncontrada;
    int costeMejorSolucionEncontrada;
    int iteracionesDesdeMejorSolucion;
    int[][] memoriaLargoPlazo;

    public SolucionBusquedaTabu(String path, long semilla) throws FileNotFoundException {
        super(path, semilla);
        listaTabu = new TabuList(this.tam/2);
        memoriaLargoPlazo = new int[this.tam][this.tam];
        iteracionesDesdeMejorSolucion = 0;
    }

    public ModelSolucion getSolucion() {

        long startTime = System.currentTimeMillis();

        int[] solucion = this.generaSolucionAleatoria();
        int costeSolucion = this.funcionObjetivo(solucion);
        setupMemoriaLargoPlazo(solucion);

        this.mejorSolucionEncontrada = solucion;
        this.costeMejorSolucionEncontrada = costeSolucion;

        for(int i = 0; i < 10000; i++){
            Intercambio intercambio = getMejorVecino(solucion, costeSolucion);
            costeSolucion += this.getDiffCoste(solucion, intercambio.getUnidadA(), intercambio.getUnidadB());

            solucion[intercambio.getUnidadA()] = intercambio.getLocalizacionA();
            solucion[intercambio.getUnidadB()] = intercambio.getLocalizacionB();

            listaTabu.insert(intercambio);
            memoriaLargoPlazo[intercambio.getUnidadA()][intercambio.getLocalizacionA()]++;
            memoriaLargoPlazo[intercambio.getUnidadB()][intercambio.getLocalizacionB()]++;

            if(costeSolucion < costeMejorSolucionEncontrada){
                costeMejorSolucionEncontrada = costeSolucion;
                mejorSolucionEncontrada = solucion;
                iteracionesDesdeMejorSolucion = 0;
            }else{
                iteracionesDesdeMejorSolucion++;
            }

            if(iteracionesDesdeMejorSolucion == 75){
                solucion = reiniciaBusqueda();
                costeSolucion = this.funcionObjetivo(solucion);

                if(costeSolucion < this.costeMejorSolucionEncontrada){
                    this.costeMejorSolucionEncontrada = costeSolucion;
                    this.mejorSolucionEncontrada = solucion;
                }
            }
        }

        return new ModelSolucion(mejorSolucionEncontrada, System.currentTimeMillis() - startTime, costeMejorSolucionEncontrada);
    }

    private int[] reiniciaBusqueda() {
        int[] nuevaSolucion;

        this.iteracionesDesdeMejorSolucion = 0;

        listaTabu.removeAll();

        if(this.random.nextBoolean())
            listaTabu.multiplyMaxSize(1.5);
        else
            listaTabu.multiplyMaxSize(0.5);

        int estrategia = this.random.nextInt(100);

        if(estrategia < SolucionBusquedaTabu.ESTRATEGIA_INTENSIFICACION)
            nuevaSolucion = this.mejorSolucionEncontrada;
        else if(estrategia < SolucionBusquedaTabu.ESTRATEGIA_DIVERSIFICACION)
            nuevaSolucion = this.generaSolucionAleatoria();
        else
            nuevaSolucion = this.generaSolucionDiversificacionControlada();

        return nuevaSolucion;
    }

    private void setupMemoriaLargoPlazo(int[] solucionInicial) {
        for(int i = 0; i < this.tam; i++)
            memoriaLargoPlazo[i][solucionInicial[i]]++;
    }

    private Intercambio getMejorVecino(int[] solucion, int costeInicial){

        int costeMejorVecino = Integer.MAX_VALUE;
        Intercambio mejorVecino = null;

        for(int i = 0; i < this.tam; i++){

            int a = this.random.nextInt(this.tam);
            int b = this.random.nextInt(this.tam);

            while(b == a) b = this.random.nextInt(this.tam);

            int coste = costeInicial + this.getDiffCoste(solucion, a, b);
            Intercambio intercambio = new Intercambio(a, b, solucion[b], solucion[a]);

            boolean isIntercambioTabu = listaTabu.isTabu(intercambio);

            if((isIntercambioTabu && coste < this.costeMejorSolucionEncontrada) || !isIntercambioTabu)
                if (costeMejorVecino > coste) {
                    mejorVecino = intercambio;
                    costeMejorVecino = coste;
                }

        }

        return mejorVecino;
    }

    private int[] generaSolucionDiversificacionControlada(){
        int[] nuevaSolucion = new int[this.tam];

        ArrayList<Integer> localizacionesAsignadas = new ArrayList<Integer>();

        for (int i = 0; i < nuevaSolucion.length; i++){
            int localizacionMenosAsignada = 0;
            ArrayList<Integer> menores = new ArrayList<Integer>();

            while(localizacionesAsignadas.contains(localizacionMenosAsignada)){
                localizacionMenosAsignada++;
            }

            menores.add(localizacionMenosAsignada);

            for(int j = localizacionMenosAsignada+1; j < nuevaSolucion.length; j++){
                if(this.memoriaLargoPlazo[i][j] <= this.memoriaLargoPlazo[i][localizacionMenosAsignada] && !localizacionesAsignadas.contains(j)){
                    if(this.memoriaLargoPlazo[i][j] == this.memoriaLargoPlazo[i][localizacionMenosAsignada]){
                        menores.add(j);
                    }else {
                        localizacionMenosAsignada = j;
                        menores.clear();
                        menores.add(localizacionMenosAsignada);
                    }
                }
            }
            nuevaSolucion[i] = menores.get(this.random.nextInt(menores.size()));
            this.memoriaLargoPlazo[i][nuevaSolucion[i]]++;
            localizacionesAsignadas.add(nuevaSolucion[i]);
        }

        return nuevaSolucion;
    }
}
