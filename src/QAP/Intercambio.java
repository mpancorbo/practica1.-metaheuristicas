package QAP;

/**
 * Created by mpancorbo on 08/10/14.
 */
public class Intercambio {
    private int localizacionA;
    private int localizacionB;
    private int unidadA;
    private int unidadB;

    public Intercambio(int _unidadA, int _unidadB, int _localizacionA, int _localizacionB){
        this.localizacionA = _localizacionA;
        this.localizacionB = _localizacionB;
        this.unidadA = _unidadA;
        this.unidadB = _unidadB;
    }

    public int getLocalizacionA() {
        return localizacionA;
    }

    public int getLocalizacionB() {
        return localizacionB;
    }

    public int getUnidadA() {
        return unidadA;
    }

    public int getUnidadB() {
        return unidadB;
    }
}
