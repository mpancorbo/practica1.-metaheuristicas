package QAP;

/**
 * Created by mpancorbo on 07/10/14.
 */
public class ModelSolucion {
    private long timeElapsed;
    private int[] permutacion;
    private int coste;

    public ModelSolucion(int[] _permutacion, long _timeElapsed, int _coste){
        this.coste = _coste;
        this.permutacion = _permutacion;
        this.timeElapsed = _timeElapsed;
    }

    public long getTimeElapsed() {
        return timeElapsed;
    }

    public void setTimeElapsed(long timeElapsed) {
        this.timeElapsed = timeElapsed;
    }

    public int[] getPermutacion() {
        return permutacion;
    }

    public void setPermutacion(int[] permutacion) {
        this.permutacion = permutacion;
    }

    public int getCoste() {
        return coste;
    }

    public void setCoste(int coste) {
        this.coste = coste;
    }

    public void printSolucion(){
        for(int i = 0; i < permutacion.length; i++)
            System.out.print(permutacion[i] + "\t");

        System.out.println();
    }
}
