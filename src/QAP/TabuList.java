package QAP;

import java.util.Stack;
/**
 * Created by mpancorbo on 08/10/14.
 */
public class TabuList {

    private Stack<Intercambio> stack;
    private int stackMaxSize;

    public TabuList(int maxSize){
        this.stackMaxSize = maxSize;
        stack = new Stack<Intercambio>();
    }

    public void insert(Intercambio item){

        stack.push(item);

        if(stack.size() > stackMaxSize)
            stack.pop();
    }

    /*public boolean isTabu(Intercambio item){

        for(Intercambio stackItem : stack){
            if(item.getLocalizacionA() == stackItem.getLocalizacionA() && item.getUnidadA() == stackItem.getUnidadA())

                return true;
            if(item.getLocalizacionB() == stackItem.getLocalizacionB() && item.getUnidadB() == stackItem.getUnidadB())

                return true;
        }

        return false;
    }*/

    public boolean isTabu(Intercambio item){

        for(Intercambio stackItem : stack){
            if(item.getLocalizacionA() == stackItem.getLocalizacionA() && item.getUnidadA() == stackItem.getUnidadA())
                if(item.getLocalizacionB() == stackItem.getLocalizacionB() && item.getUnidadB() == stackItem.getUnidadB())
                    return true;
        }

        return false;
    }

    public void removeAll(){
        for (int i = 0; i < stack.size(); i++) {
            stack.remove(i);
        }
    }

    public void multiplyMaxSize(double newSize){
        this.stackMaxSize *= newSize;

        while(this.stack.size() > this.stackMaxSize)
            this.stack.pop();
    }

}
