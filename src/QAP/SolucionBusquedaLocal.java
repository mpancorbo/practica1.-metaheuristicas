package QAP;

import java.io.FileNotFoundException;

/**
 * Created by mpancorbo on 02/10/14.
 */
public class SolucionBusquedaLocal extends AbstractSolucion {

    public SolucionBusquedaLocal(String path, long semilla) throws FileNotFoundException{
        super(path, semilla);
    }

    public ModelSolucion getSolucion(){
        long timeStart = System.currentTimeMillis();

        int[] solucion = generaSolucionAleatoria();

        int dontLookBits[] = new int[this.tam];

        int coste = funcionObjetivo(solucion);
        boolean mejora;
        do {
            mejora = false;
            for (int i = 0; i < this.tam; i++) {
                if (dontLookBits[i] == 0 && !mejora) {
                    for (int j = 0; j < this.tam; j++) {
                        if (i != j && !mejora) {
                            int diff = getDiffCoste(solucion, i, j);

                            if (diff < 0) {
                                mejora = true;
                                coste += diff;

                                int aux = solucion[i];
                                solucion[i] = solucion[j];
                                solucion[j] = aux;

                                dontLookBits[i] = 0;
                                dontLookBits[j] = 0;
                            }
                        }
                    }
                    if (!mejora) dontLookBits[i] = 1;
                }
            }
        }while(mejora);

        return new ModelSolucion(solucion, System.currentTimeMillis() - timeStart, coste);
    }

}
