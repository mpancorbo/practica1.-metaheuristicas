package QAP;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Random;

/**
 * Created by mpancorbo on 07/10/14.
 */
public abstract class AbstractSolucion {

    private static final int ORDEN_FLUJOS_DISTANCIAS = 0;
    private static final int ORDEN_DISTANCIAS_FLUJOS = 1;

    protected int tam;
    protected int flujo[][];
    protected int distancia[][];
    protected Random random;

    public abstract ModelSolucion getSolucion();

    public AbstractSolucion(String path, long semilla) throws FileNotFoundException{
        Scanner scanner = new Scanner(new File(path));
        this.tam = scanner.nextInt();
        int orden = scanner.nextInt();
        if(orden == ORDEN_DISTANCIAS_FLUJOS){
            setMatrizDistancias(scanner);
            setMatrizFlujos(scanner);
        }else if(orden == ORDEN_FLUJOS_DISTANCIAS){
            setMatrizFlujos(scanner);
            setMatrizDistancias(scanner);
        }
        random = new Random(semilla);
    }
    public AbstractSolucion(String path) throws FileNotFoundException{
        Scanner scanner = new Scanner(new File(path));
        this.tam = scanner.nextInt();
        int orden = scanner.nextInt();
        if(orden == ORDEN_DISTANCIAS_FLUJOS){
            setMatrizDistancias(scanner);
            setMatrizFlujos(scanner);
        }else if(orden == ORDEN_FLUJOS_DISTANCIAS){
            setMatrizFlujos(scanner);
            setMatrizDistancias(scanner);
        }
    }

    private void setMatrizFlujos(Scanner scanner) {
        this.flujo= new int[this.tam][this.tam];
        for(int i = 0; i < this.tam; i++)
            for(int j = 0; j < this.tam; j++)
                this.flujo[i][j] = scanner.nextInt();
    }

    private void setMatrizDistancias(Scanner scanner) {
        this.distancia = new int[this.tam][this.tam];

        for(int i = 0; i < this.tam; i++)
            for(int j = 0; j < this.tam; j++)
                this.distancia[i][j] = scanner.nextInt();
    }

    protected int funcionObjetivo(int[] solucion){
        int coste = 0;

        for(int i = 0; i < solucion.length; i++)
            for(int j = 0; j < solucion.length; j++)
                if(i != j)
                    coste += flujo[i][j]*distancia[solucion[i]][solucion[j]];

        return coste;
    }

    protected int[] generaSolucionAleatoria(){
        int[] solucionAleatoria = new int[this.tam];

        for(int i = 0; i < solucionAleatoria.length; i++){
            solucionAleatoria[i] = i;
        }

        int limite = solucionAleatoria.length-1;

        for(int i = limite; i > 0; i--) {
            int randomIndex = this.random.nextInt(i);
            int aux = solucionAleatoria[i];
            solucionAleatoria[i] = solucionAleatoria[randomIndex];
            solucionAleatoria[randomIndex] = aux;
        }


        return solucionAleatoria;
    }

    protected int getDiffCoste(int[] solucion, int a, int b){

        int diff = 0;

        if (a == b)
            return 0;

        for(int i = 0; i < solucion.length; i++){
            if(i!=a && i!=b)
                diff += this.flujo[a][i]*(this.distancia[solucion[b]][solucion[i]] - this.distancia[solucion[a]][solucion[i]])
                        + this.flujo[b][i]*(this.distancia[solucion[a]][solucion[i]] - this.distancia[solucion[b]][solucion[i]])
                        + this.flujo[i][a]*(this.distancia[solucion[i]][solucion[b]] - this.distancia[solucion[i]][solucion[a]])
                        + this.flujo[i][b]*(this.distancia[solucion[i]][solucion[a]] - this.distancia[solucion[i]][solucion[b]]);
        }

        return diff;
    }
}
