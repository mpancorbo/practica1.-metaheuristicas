# README #

Práctica 1 para la asignatura Metaheurísticas, correspondiente al tercer curso del Grado en Ingeniería Informática de la Universidad de Jaén.

### ¿En qué consiste esta práctica? ###

El objetivo de la práctica es el de resolver problemas QAP (Quadratic Assignment Problem) usando soluciones basadas en metaheurísticas, que si bien no tienen el objetivo de encontrar la solución óptima, optan por encontrar una solución razonablemente buena en un tiempo pequeño.

* Solución basada en algoritmos voraces (Greedy)
* Solución basada en búsqueda local (primero el mejor)
* Solución basada en búsqueda tabú

### Estado actual ###

* Solución basada en algoritmos voraces.
* Solución basada en búsqueda local.
* Solución basada en búsqueda tabú.
* Interfaz que te permite por consola seleccionar:
       * Problema a resolver.
       * Tipo de metodología a usar para resolver el problema.
       * Semilla para la generación aleatoria soluciones.
       * Cantidad de veces que se va a resolver el problema.
       * Resolver un problema con todos los métodos disponibles.
       * Resolver todos los problemas disponibles con todos los métodos disponibles.

### Alumnos ###

* Manuel Pancorbo Pestaña (a.k.a. mpp00017)
* Manuel José Castro Damas (a.k.a. Xuela08)